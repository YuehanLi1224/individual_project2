FROM rust:1.75 as builder


WORKDIR /usr/src/individual-project2

COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

RUN mkdir src/ && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/individual-project2*

COPY ./src ./src

# Build for release
RUN cargo build --release

FROM debian:bookworm-slim
WORKDIR /usr/local/bin

COPY --from=builder /usr/src/individual-project2/target/release/individual-project2 .

# Set permissions to execute the binary
RUN chmod +x ./individual-project2

# Run the binary
CMD ["./individual-project2"]
