
.PHONY: lint format test build

# Lint the project
lint:
	cargo clippy -- -D warnings

# Format the project
format:
	cargo fmt

# Test the project
test:
	cargo test

# Build the project
build:
	cargo build --release
