use actix_web::{get, App, HttpRequest, HttpServer, Responder};

#[get("/")]
async fn index(req: HttpRequest) -> impl Responder {
    // Extract query parameters
    let params = req.query_string();

    // Parse query parameters
    let mut x1: Option<i32> = None;
    let mut x2: Option<i32> = None;
    for (key, value) in serde_urlencoded::from_str::<Vec<(String, i32)>>(params).unwrap() {
        match key.as_str() {
            "x1" => x1 = Some(value),
            "x2" => x2 = Some(value),
            _ => (),
        }
    }

    let sum = match (x1, x2) {
        (Some(x1), Some(x2)) => x1 + x2,
        _ => 0, // Default value if parameters are missing
    };

    // Return response
    format!("Sum of x1 and x2 is {}", sum)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(index))
    .bind("127.0.0.1:8080")?
    .run()
    .await
}

